// Discussion 3: Functions
// By San Myo Aung

#include <iostream>
#include <iomanip>
using namespace std;

const int FREEZE_POINT = 32;
bool toCelsiusByReference(float &);

int main()
{
  float temperature;
  bool freezing;
  cout << "Welcome to the temperature converter!" << endl;
  cout << "Please enter a temperature in Fahrenheit: ";
  cin >> temperature;
  toCelsiusByReference(temperature);
  cout.setf(ios::fixed, ios::floatfield);
  cout << "The equivalent Celsius is: " << setprecision(1) << temperature << endl;
  if (freezing)
    cout << "This temperature is below freezing" << endl;
  else
    cout << "This temperature is above freezing" << endl;

  return 0;
}

bool toCelsiusByReference(float &fTemp)
{
  bool freezing;
  if (fTemp <= FREEZE_POINT)
    freezing = true;
  else
    freezing = false;
  fTemp = (float)5 / 9 * (fTemp - FREEZE_POINT);
  return freezing;
}

/*

*/