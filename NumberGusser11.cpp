// Assignment 11: Number Guesser Class
// By San Myo Aung

#include <iostream>
#include "NumberGuesser.h"

using namespace std;

void playOneGame();
bool shouldPlayAgain();

int main()
{
  do
  {
    playOneGame();
  } while (shouldPlayAgain());

  cout << "Goodbye." << endl;
  return 0;
}

void playOneGame()
{
  NumberGuesser guesser(1, 100); // Create a NumberGuesser object
  char response;

  cout << "Think of a number between 1 and 100." << endl;

  do
  {
    cout << "Is the number " << guesser.getCurrentGuess() << "? (h/l/c): ";
    cin >> response;

    switch (response)
    {
    case 'h':
      guesser.higher();
      break;
    case 'l':
      guesser.lower();
      break;
    case 'c':
      cout << "You picked " << guesser.getCurrentGuess()
           << "? Great pick." << endl;
      break;
    }
  } while (response != 'c');
}

bool shouldPlayAgain()
{
  char response;
  cout << "Do you want to play again? (y/n): ";
  cin >> response;
  return (response == 'y' || response == 'Y');
}

/*
[saung32@hills ccsf-c-programming]$ ./a.out
Think of a number between 1 and 100.
Is the number 50? (h/l/c): h
Is the number 75? (h/l/c): h
Is the number 88? (h/l/c): l
Is the number 81? (h/l/c): l
Is the number 78? (h/l/c): l
Is the number 76? (h/l/c): h
Is the number 77? (h/l/c): c
You picked 77? Great pick.
Do you want to play again? (y/n): y
Think of a number between 1 and 100.
Is the number 50? (h/l/c): l
Is the number 25? (h/l/c): l
Is the number 12? (h/l/c): h
Is the number 18? (h/l/c): h
Is the number 21? (h/l/c): h
Is the number 23? (h/l/c): l
Is the number 22? (h/l/c): c
You picked 22? Great pick.
Do you want to play again? (y/n): n
Goodbye.
[saung32@hills ccsf-c-programming]$
*/