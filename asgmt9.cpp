// Assignment 9: The league with structs
// By San Myo Aung

#include <iostream>
#include <cstring>
#include <iomanip>
using namespace std;

// WinRecord struct definition
struct WinRecord
{
  int wins;
  char *name;
};

// Function prototypes (modified to use WinRecord pointers)
void initializeData(WinRecord *standings, int size);
void sortData(WinRecord *standings, int size);
void displayData(WinRecord *standings, int size);
void swap(WinRecord &a, WinRecord &b);
char *getLine();

int main()
{
  int numTeams;

  cout << "How many teams are in the league? ";
  cin >> numTeams;

  // Dynamically allocate an array of WinRecord structs
  WinRecord *standings = new WinRecord[numTeams];

  // Initialize the data
  initializeData(standings, numTeams);

  // Sort the data
  sortData(standings, numTeams);

  // Display the data
  displayData(standings, numTeams);

  // Deallocate memory for the names (C-strings)
  for (int i = 0; i < numTeams; i++)
  {
    delete[] standings[i].name;
  }

  // Deallocate memory for the array of WinRecord structs
  delete[] standings;

  return 0;
}

// Function to initialize the data
void initializeData(WinRecord *standings, int size)
{
  for (int i = 0; i < size; i++)
  {
    cout << "Enter team #" << i + 1 << ": ";
    cin.ignore(); // To clear the buffer after reading the number of teams
    standings[i].name = getLine();
    cout << "Enter the wins for team #" << i + 1 << ": ";
    cin >> standings[i].wins;
  }
}

// Function to sort the data (using selection sort)
void sortData(WinRecord *standings, int size)
{
  int maxIndex;

  for (int s = 0; s < size - 1; s++)
  {
    maxIndex = s;
    for (int i = s + 1; i < size; i++)
    {
      if (standings[i].wins > standings[maxIndex].wins)
      {
        maxIndex = i;
      }
    }
    if (maxIndex != s)
    {
      swap(standings[maxIndex], standings[s]);
    }
  }
}

// Function to display the data
void displayData(WinRecord *standings, int size)
{
  cout << "\nLeague Standings:\n";
  for (int i = 0; i < size; i++)
  {
    cout << standings[i].name << ": " << standings[i].wins << endl;
  }
}

// Swap function for WinRecord structs
void swap(WinRecord &a, WinRecord &b)
{
  WinRecord temp = a;
  a = b;
  b = temp;
}

char *getLine()
{
  const int BUFFER_SIZE = 100;
  char buffer[BUFFER_SIZE];
  cin.getline(buffer, BUFFER_SIZE);
  int length = strlen(buffer);

  char *line = new char[length + 1];
  strcpy(line, buffer);
  return line;
}

/*
[saung32@hills ccsf-c-programming]$ ./a.out
How many teams are in the league? 4
Enter team #1: Pico
Enter the wins for team #1: 76
Enter team #2: Rockers
Enter the wins for team #2: 68
Enter team #3: Dodge
Enter the wins for team #3: 93
Enter team #4: Giants
Enter the wins for team #4: 94

League Standings:
Giants: 94
Dodge: 93
Pico: 76
Rockers: 68
[saung32@hills ccsf-c-programming]$
*/