# Get input for the first person
name1 = input("Please enter the name of one person: ")
age1 = int(input(f"Please enter {name1}'s age: "))

# Get input for the second person
name2 = input("Please enter the name of another person: ")
age2 = int(input(f"Please enter {name2}'s age: "))

# Use Boolean variables for clarity
person1_in_range = 18 <= age1 <= 64
person2_in_range = 18 <= age2 <= 64

# Determine the appropriate output message
if person1_in_range and person2_in_range:
    print("Both people are between 18 and 64")
elif person1_in_range:
    print(f"{name1} is between 18 and 64, but {name2} is not.")
elif person2_in_range:
    print(f"{name2} is between 18 and 64, but {name1} is not.")
else:
    print("Neither person is between 18 and 64")