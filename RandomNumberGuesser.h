// Assignment 14 - Random Number Guesser
// By San Myo Aung

// RandomNumberGuesser.h
#ifndef RANDOMNUMBERGUESSER_H
#define RANDOMNUMBERGUESSER_H

#include "NumberGuesser.h"
#include <random>

class RandomNumberGuesser : public NumberGuesser
{
private:
  int currentGuess;
  std::random_device rd;
  std::mt19937 gen;
  std::uniform_int_distribution<> distrib;

public:
  RandomNumberGuesser(int l, int h);
  int getCurrentGuess();
  void higher();
  void lower();
  void reset();
};

#endif