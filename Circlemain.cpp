// Assignment 10: Circle Class
// By San Myo Aung

#include <iostream>
#include "Circle.h"

using namespace std;

int main()
{
  // Local circle object
  double x = 0, y = 0, r = 0;
  cout << "Set X : ";
  cin >> x;
  cout << "Set Y : ";
  cin >> y;
  cout << "Set radius : ";
  cin >> r;

  Circle myCircle(x, y, r);
  cout << "Area of myCircle: " << myCircle.getArea() << endl;

  // Circle pointer
  Circle *circlePtr = &myCircle;
  cout << "Set new X : ";
  cin >> x;
  cout << "Set new Y : ";
  cin >> y;
  cout << "Set new radius : ";
  cin >> r;
  circlePtr->setX(x);
  circlePtr->setY(y);
  circlePtr->setRadius(r);

  // Test containsPoint()
  cout << "Contains (2.0, 4.0): " << (circlePtr->containsPoint(2.0, 4.0) ? "true" : "false") << endl;
  cout << "Contains (2.0, 10.0): " << (circlePtr->containsPoint(2.0, 10.0) ? "true" : "false") << endl;

  return 0;
}

/*
[saung32@hills ccsf-c-programming]$ ./a.out
Set X : 2
Set Y : 3
Set radius : 2
Area of myCircle: 12.56
Set new X : 1
Set new Y : 2
Set new radius : 3
Contains (2.0, 4.0): true
Contains (2.0, 10.0): false
[saung32@hills ccsf-c-programming]$ ./a.out
Set X : 2
Set Y : 4
Set radius : 3
Area of myCircle: 28.26
Set new X : 1
Set new Y : 5
Set new radius : 6
Contains (2.0, 4.0): true
Contains (2.0, 10.0): true
[saung32@hills ccsf-c-programming]$ ./a.out
Set X : 1
Set Y : 1
Set radius : 1
Area of myCircle: 3.14
Set new X : 1
Set new Y : 1
Set new radius : 1
Contains (2.0, 4.0): false
Contains (2.0, 10.0): false
[saung32@hills ccsf-c-programming]$
*/