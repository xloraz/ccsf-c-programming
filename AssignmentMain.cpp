#include <iostream>
#include "Assignment.h"
using namespace std;

int main()
{
  Assignment assignment;
  int f, e, s;

  cout << "Enter the functionality score (max 50): ";
  cin >> f;
  cout << "Enter the efficiency score (max 25): ";
  cin >> e;
  cout << "Enter the style score (max 25): ";
  cin >> s;

  assignment.set(f, e, s);

  cout << "Total score: " << assignment.getScore() << endl;
  cout << "Letter grade: " << assignment.getLetterGrade() << endl;

  return 0;
}

/*

Sample Out

Enter the functionality score (max 50): 30
Enter the efficiency score (max 25): 21
Enter the style score (max 25): 20
Total score: 71
Letter grade: C

*/