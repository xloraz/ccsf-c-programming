// Assignment 2: San Myo Aung
// Blackjack

#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

// Get a card
int getCard()
{
  return (rand() % 10) + 1; // return random value from 1 to 10
}

int main()
{
  char tryAgain = 'y';
  while (tryAgain != 'n')
  {
    int card, pHand = 0, dHand = 0; // new card, player's hand and dealer's heand

    // Get the system time
    unsigned seed = time(0);

    // Seed the number generator
    srand(seed);

    card = getCard(); // get a new card
    cout << "Dealer starts with a " << card << endl;
    dHand += card;    // added to dealer
    card = getCard(); // get a new card
    cout << "Your first cards: " << card;
    pHand += card;    // added to player
    card = getCard(); // get a new card
    cout << ", " << card << endl;
    pHand += card; // added to player
    cout << "Your Hand: " << pHand << endl;
    bool pBust = false;
    while (true)
    {
      cout << "Hit? (y/n): ";
      char hit;
      cin >> hit;
      if (hit == 'y')
      {
        card = getCard();
        cout << "Card: " << card << endl;
        pHand += card;
        cout << "Player's Total: " << pHand << endl;
        if (pHand > 21)
        {
          pBust = true;
          cout << "Bust!" << endl
               << "Dealer wins!" << endl;
          break;
        }
      }
      else if (hit == 'n')
      {
        break;
      }
      else
        cout << "INVALID CHOICE! Try again... \n";
    }

    // if player is not busted
    if (!pBust)
    {
      cin.ignore();
      cout << "Dealer has a " << dHand << "...\n";
      while (true)
      {
        cout << "(enter to continue) ";
        cin.get();
        card = getCard(); // get a new card
        cout << "Card: " << card << endl;
        dHand += card; // added to dealer
        cout << "Dealer's Total: " << dHand << endl;

        // check if dealer is busted
        if (dHand > 21)
        {
          cout << "Bust!" << endl
               << "Player wins!" << endl;
          break;
        }
        else if (dHand >= 17) // if dealer does not need to hit more
        {
          if (dHand == pHand) // if tie
          {

            cout << "Push!" << endl;
            break;
          }
          else if (dHand > pHand) // check who wins
          {
            cout << "Dealer wins!" << endl;
            break;
          }
          else
          {
            cout << "Player wins!" << endl;
            break;
          }
        }
      }
    }
    cout << "Try again? (y/n): ";
    cin >> tryAgain;
    cout << endl;
  }
  return 0;
}

/*

[saung32@hills ccsf-c-programming]$ ./a.out
Dealer starts with a 5
Your first cards: 3, 4
Your Hand: 7
Hit? (y/n): y
Card: 1
Player's Total: 8
Hit? (y/n): y
Card: 9
Player's Total: 17
Hit? (y/n): y
Card: 2
Player's Total: 19
Hit? (y/n): n
Dealer has a 5...
(enter to continue)
Card: 1
Dealer's Total: 6
(enter to continue)
Card: 6
Dealer's Total: 12
(enter to continue)
Card: 5
Dealer's Total: 17
Player wins!
Try again? (y/n): y

Dealer starts with a 10
Your first cards: 2, 10
Your Hand: 12
Hit? (y/n): y
Card: 2
Player's Total: 14
Hit? (y/n): y
Card: 8
Player's Total: 22
Bust!
Dealer wins!
Try again? (y/n): y

Dealer starts with a 2
Your first cards: 8, 10
Your Hand: 18
Hit? (y/n): n
Dealer has a 2...
(enter to continue)
Card: 10
Dealer's Total: 12
(enter to continue)
Card: 5
Dealer's Total: 17
Player wins!
Try again? (y/n): y

Dealer starts with a 4
Your first cards: 4, 8
Your Hand: 12
Hit? (y/n): y
Card: 3
Player's Total: 15
Hit? (y/n): y
Card: 3
Player's Total: 18
Hit? (y/n): n
Dealer has a 4...
(enter to continue)
Card: 4
Dealer's Total: 8
(enter to continue)
Card: 8
Dealer's Total: 16
(enter to continue)
Card: 1
Dealer's Total: 17
Player wins!
Try again? (y/n): n

[saung32@hills ccsf-c-programming]$

*/