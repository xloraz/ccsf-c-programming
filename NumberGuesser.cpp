#include "NumberGuesser.h"

NumberGuesser::NumberGuesser() : NumberGuesser(1, 100) {}

NumberGuesser::NumberGuesser(int lowerBound, int upperBound)
{
  this->lowerBound = lowerBound;
  this->upperBound = upperBound;
  this->originalLowerBound = lowerBound;
  this->originalUpperBound = upperBound;
}

void NumberGuesser::higher()
{
  lowerBound = getCurrentGuess() + 1;
}

void NumberGuesser::lower()
{
  upperBound = getCurrentGuess() - 1;
}

int NumberGuesser::getCurrentGuess()
{
  return (lowerBound + upperBound) / 2;
}

void NumberGuesser::reset()
{
  lowerBound = originalLowerBound;
  upperBound = originalUpperBound;
}