// Discussion 9: Taqueria with structs
// By San Myo Aung

#include <iostream>
#include <string>

using namespace std;

// Define the Cost struct
struct Cost
{
  double wholesale;
  double retail;
};

// Define the SalesRecord struct
struct SalesRecord
{
  string name;
  int numSold;
  Cost cost;
};

// Function to calculate profit for a single SalesRecord
float getProfit(SalesRecord record)
{
  double unitProfit = record.cost.retail - record.cost.wholesale;
  return unitProfit * record.numSold;
}

// Function to print the profit report
void printReport(SalesRecord *records, int size)
{
  double totalProfit = 0;

  cout << "\nDaily Sales Report:\n";
  for (int i = 0; i < size; i++)
  {
    float profit = getProfit(records[i]);
    cout << records[i].name << ": " << records[i].numSold
         << " sold, Profit: $" << profit << endl;
    totalProfit += profit;
  }
  cout << "---------------------\n";
  cout << "Total Profit: $" << totalProfit << endl;
}

int main()
{
  int numTypes;

  cout << "Enter the number of burrito types: ";
  cin >> numTypes;

  // Dynamically allocate an array of SalesRecord structs
  SalesRecord *records = new SalesRecord[numTypes];

  for (int i = 0; i < numTypes; i++)
  {
    cout << "Enter the name of burrito type " << i + 1 << ": ";
    cin >> records[i].name;
    cout << "Enter the number of " << records[i].name << " burritos sold: ";
    cin >> records[i].numSold;
    cout << "Enter the wholesale cost of " << records[i].name << ": ";
    cin >> records[i].cost.wholesale;
    cout << "Enter the retail cost of " << records[i].name << ": ";
    cin >> records[i].cost.retail;
  }

  // Call the printReport function
  printReport(records, numTypes);

  // Deallocate the dynamically allocated array
  delete[] records;

  return 0;
}

/*
[saung32@hills ccsf-c-programming]$ ./a.out
Enter the number of burrito types: 4
Enter the name of burrito type 1: Carnitas
Enter the number of Carnitas burritos sold: 4
Enter the wholesale cost of Carnitas: 25
Enter the retail cost of Carnitas: 27
Enter the name of burrito type 2: Pollo
Enter the number of Pollo burritos sold: 17
Enter the wholesale cost of Pollo: 19
Enter the retail cost of Pollo: 21
Enter the name of burrito type 3: Veggie
Enter the number of Veggie burritos sold: 10
Enter the wholesale cost of Veggie: 15
Enter the retail cost of Veggie: 17
Enter the name of burrito type 4: Fish
Enter the number of Fish burritos sold: 15
Enter the wholesale cost of Fish: 12
Enter the retail cost of Fish: 15

Daily Sales Report:
Carnitas: 4 sold, Profit: $8
Pollo: 17 sold, Profit: $34
Veggie: 10 sold, Profit: $20
Fish: 15 sold, Profit: $45
---------------------
Total Profit: $107
[saung32@hills ccsf-c-programming]$
*/