// Assignment 5: San Myo Aung
// The League

#include <iostream>
#include <iomanip>
using namespace std;

void initializeArrays(string[], int[], int);
void sortData(string[], int[], int);
void displayData(string[], int[], int);
void swap(int &, int &);
void swap(string &, string &);

int main()
{
  const int NUM = 5;
  string names[NUM];
  int wins[NUM];
  initializeArrays(names, wins, NUM);
  sortData(names, wins, NUM);
  displayData(names, wins, NUM);
  return 0;
}

void initializeArrays(string names[], int wins[], int size)
{
  for (int i = 0; i < size; i++)
  {
    cout << "Enter team #" << i + 1 << ": ";
    cin >> names[i];
    cout << "Enter the wins for team #" << i + 1 << ": ";
    cin >> wins[i];
  }
}

void sortData(string names[], int wins[], int size)
{
  int s, maxIndex, maxValue;

  for (s = 0; s < (size - 1); s++)
  {
    maxIndex = s;
    maxValue = wins[s];
    for (int i = s + 1; i < size; i++)
    {
      if (wins[i] > maxValue)
      {
        maxValue = wins[i];
        maxIndex = i;
      }
    }
    swap(wins[maxIndex], wins[s]);
    swap(names[maxIndex], names[s]);
  }
}

void displayData(string names[], int wins[], int size)
{
  cout << "\nLeague Standings:\n";
  for (int i = 0; i < size; i++)
  {
    cout << names[i] << ": " << wins[i] << endl;
  }
}

void swap(int &a, int &b)
{
  int temp = a;
  a = b;
  b = temp;
}

void swap(string &a, string &b)
{
  string temp = a;
  a = b;
  b = temp;
}
/*

[saung32@hills ccsf-c-programming]$ ./a.out
Enter team #1: Pico
Enter the wins for team #1: 76
Enter team #2: Rockers
Enter the wins for team #2: 68
Enter team #3: Dodge
Enter the wins for team #3: 93
Enter team #4: Giants
Enter the wins for team #4: 94
Enter team #5: Diamond
Enter the wins for team #5: 71

League Standings:
Giants: 94
Dodge: 93
Pico: 76
Diamond: 71
Rockers: 68
[saung32@hills ccsf-c-programming]$

*/