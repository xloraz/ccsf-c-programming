// Discussion 7: The taqueria revisited
// By San Myo Aung

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
  const int NUM_TYPES = 3;
  // Define the initial arrays

  string names[NUM_TYPES] = {"Carnitas", "Pollo", "Veggie"};
  float prices[NUM_TYPES] = {6.95, 6.25, 5.95};

  // Define the pointers
  string *namePtr = names;
  float *pricePtr = prices;

  // Add " Especial" to the name and 2.00 to the prices using a for loop
  for (int i = 0; i < NUM_TYPES; i++)
  {
    string type = *(namePtr + i);
    type = type + " Especial";
    *(namePtr + i) = type;
    *(pricePtr + i) += 2.0;
  }

  // Print out new menu using a for loop
  cout << "New Menu:" << endl;
  for (int i = 0; i < NUM_TYPES; i++)
  {
    cout << *(namePtr + i) << " : $" << *(pricePtr + i) << endl;
  }

  return 0;
}

/*
[saung32@hills ccsf-c-programming]$ ./a.out
New Menu:
Carnitas Especial : $8.95
Pollo Especial : $8.25
Veggie Especial : $7.95
[saung32@hills ccsf-c-programming]$
*/