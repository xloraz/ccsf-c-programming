// Discussion 13: Overloaded operators and aggregation
// By San Myo Aung

#include <iostream>
#include <cstring> // For strcpy

class Rectangle
{
private:
  double width;
  double length;
  char *name;
  void initName(char *n);

public:
  Rectangle();
  Rectangle(double w, double l, char *n);
  Rectangle(const Rectangle &r); // Copy Constructor
  ~Rectangle();

  Rectangle &operator=(const Rectangle &r);
  bool operator<(const Rectangle &r) const;
  bool operator>(const Rectangle &r) const;

  void print() const;
};

// Private function to initialize the name (avoid code duplication)
void Rectangle::initName(char *n)
{
  name = new char[strlen(n) + 1];
  strcpy(name, n);
}

// Default Constructor
Rectangle::Rectangle() : width(0), length(0), name(nullptr) {}

// Parameterized Constructor
Rectangle::Rectangle(double w, double l, char *n) : width(w), length(l)
{
  initName(n);
}

// Copy Constructor
Rectangle::Rectangle(const Rectangle &r) : width(r.width), length(r.length)
{
  initName(r.name);
}

// Destructor
Rectangle::~Rectangle()
{
  delete[] name;
}

// Overloaded assignment operator
Rectangle &Rectangle::operator=(const Rectangle &r)
{
  if (this != &r)
  {                // Avoid self-assignment
    delete[] name; // Deallocate old name
    width = r.width;
    length = r.length;
    initName(r.name);
  }
  return *this;
}

// Overloaded less than operator (compares area)
bool Rectangle::operator<(const Rectangle &r) const
{
  return (width * length) < (r.width * r.length);
}

// Overloaded greater than operator (compares area)
bool Rectangle::operator>(const Rectangle &r) const
{
  return (width * length) > (r.width * r.length);
}

// Print function
void Rectangle::print() const
{
  std::cout << "Rectangle '" << name << "' - Width: " << width
            << ", Length: " << length << std::endl;
}

int main()
{
  Rectangle r1(5, 10, (char *)"Rectangle 1");
  Rectangle r2(3, 8, (char *)"Rectangle 2");
  Rectangle r3;

  r3 = r1; // Assignment
  r3.print();

  r3 = r2 = r1; // Multiple assignments
  r3.print();

  Rectangle r4(5, 10, (char *)"Rectangle 4");
  Rectangle r5(3, 8, (char *)"Rectangle 5");

  if (r4 < r5)
  {
    std::cout << "Rectangle 4 is smaller than Rectangle 5\n";
  }
  else if (r4 > r5)
  {
    std::cout << "Rectangle 4 is larger than Rectangle 5\n";
  }
  else
  {
    std::cout << "Rectangle 4 and Rectangle 5 have the same area\n";
  }

  return 0;
}

/*
[saung32@hills ccsf-c-programming]$ ./a.out
Rectangle 'Rectangle 1' - Width: 5, Length: 10
Rectangle 'Rectangle 1' - Width: 5, Length: 10
Rectangle 4 is larger than Rectangle 5
*/