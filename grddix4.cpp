// Discussion 4: The taqueria
// By San Myo Aung

#include <iostream>
#include <iomanip>
using namespace std;

const int NUM_TYPES = 4;
string burritoType[NUM_TYPES] = {"carnitas", "beef", "shrimp", "veggie"};
int numSold[NUM_TYPES];

int main()
{
  int total = 0, x = 0;
  int higher = numSold[0];
  for (int i = 0; i < NUM_TYPES; i++)
  {
    cout << "Enter number of " << burritoType[i] << " sold: ";
    cin >> numSold[i];
    total += numSold[i];
    if (numSold[i] > higher)
    {
      higher = numSold[i];
      x = i;
    }
  }
  cout << endl;
  cout << "You sold " << total << " burritos today.  The most sold was " << burritoType[x] << "." << endl;
  return 0;
}

/*
[saung32@hills ccsf-c-programming]$ ./a.out 
Enter number of carnitas sold: 5
Enter number of beef sold: 3
Enter number of shrimp sold: 1
Enter number of veggie sold: 3

You sold 12 burritos today.  The most sold was carnitas.
*/