// Discussion 8: C-Strings
// By San Myo Aung

#include <iostream>
#include <string>
#include <cctype>

using namespace std;

int main()
{
  string password;

  cout << "Enter a password: ";
  cin >> password;

  // Check the length
  if (password.length() != 10)
  {
    cout << "Invalid password format.\n";
    return 0;
  }

  // Check the month (mmm)
  if (!islower(password[0]) || !islower(password[1]) || !islower(password[2]))
  {
    cout << "Invalid password format.\n";
    return 0;
  }

  // Check the day (DD)
  if (!isdigit(password[3]) || !isdigit(password[4]))
  {
    cout << "Invalid password format.\n";
    return 0;
  }

  // Check the year (YY)
  if (!isdigit(password[5]) || !isdigit(password[6]))
  {
    cout << "Invalid password format.\n";
    return 0;
  }

  // Check the period (.)
  if (password[7] != '.')
  {
    cout << "Invalid password format.\n";
    return 0;
  }

  // Check the initials (fl)
  if (!islower(password[8]) || !islower(password[9]))
  {
    cout << "Invalid password format.\n";
    return 0;
  }

  cout << "Valid password format.\n";
  return 0;
}

/*
[saung32@hills ccsf-c-programming]$ ./a.out
Enter a password: may2196.mk
Valid password format.
[saung32@hills ccsf-c-programming]$ ./a.out
Enter a password: Mar1212,12
Invalid password format.
[saung32@hills ccsf-c-programming]$
*/