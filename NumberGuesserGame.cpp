// Assignment 14 - Random Number Guesser
// By San Myo Aung

#include <iostream>
#include "RandomNumberGuesser.h"

using namespace std;

int main()
{
  int low, high;

  cout << "Enter the lower bound: ";
  cin >> low;
  cout << "Enter the upper bound: ";
  cin >> high;

  RandomNumberGuesser guesser(low, high);

  char response;
  do
  {
    int guess = guesser.getCurrentGuess();
    cout << "My guess is: " << guess << endl;

    cout << "Is the number higher (h), lower (l), or correct (c)? ";
    cin >> response;

    if (response == 'h')
    {
      guesser.higher();
    }
    else if (response == 'l')
    {
      guesser.lower();
    }

  } while (response != 'c');

  cout << "I guessed it!" << endl;

  return 0;
}