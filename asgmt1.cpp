// Assignment 1: San Myo Aung
// Ages

#include <iostream>
using namespace std;

int main()
{
  // Constants for ages
  const int T_AGE = 13,
            A_AGE = 18,
            R_AGE = 65;
  int age; // User's age

  // Get the age of the user
  cout << "How old are you?: ";
  cin >> age;

  // Tell the user if they are a child, a teen, an adult, or a retiree
  if (age >= R_AGE)
    cout << "You are a retiree.";
  else if (age >= A_AGE)
    cout << "You are an adult.";
  else if (age >= T_AGE)
    cout << "You are a teen.";
  else if (age >= 0)
    cout << "You are a child.";
  else
    cout << "ERROR! Age must be at least 0.";

  cout << endl;
  return 0;
}

/*

[saung32@hills ccsf-c-programming]$ ./a.out
How old are you?: 0
You are a child.
[saung32@hills ccsf-c-programming]$ ./a.out
How old are you?: -1
ERROR! Age must be at least 0.
[saung32@hills ccsf-c-programming]$ ./a.out
How old are you?: 5
You are a child.
[saung32@hills ccsf-c-programming]$ ./a.out
How old are you?: 13
You are a teen.
[saung32@hills ccsf-c-programming]$ ./a.out
How old are you?: 12
You are a child.
[saung32@hills ccsf-c-programming]$ ./a.out
How old are you?: 18
You are an adult.
[saung32@hills ccsf-c-programming]$ ./a.out
How old are you?: 65
You are a retiree.
[saung32@hills ccsf-c-programming]$ ./a.out
How old are you?: 17
You are a teen.
[saung32@hills ccsf-c-programming]$

*/