// IntCollection class implementation
// CS 110B Max Luttrell

#include "IntCollection.h"
#include <iostream>
using namespace std;

IntCollection::IntCollection()
{
  size = capacity = 0;
  data = NULL;
}

// Destructor
IntCollection::~IntCollection()
{
  delete[] data;
}

// Copy Constructor
IntCollection::IntCollection(const IntCollection &c)
{
  size = c.size;
  capacity = c.capacity;
  data = new int[capacity];
  for (int i = 0; i < size; i++)
  {
    data[i] = c.data[i];
  }
}

// Overloaded assignment operator
IntCollection &IntCollection::operator=(const IntCollection &c)
{
  if (this != &c)
  {
    delete[] data;
    size = c.size;
    capacity = c.capacity;
    data = new int[capacity];
    for (int i = 0; i < size; i++)
    {
      data[i] = c.data[i];
    }
  }
  return *this;
}

// Overloaded is equals operator
bool IntCollection::operator==(const IntCollection &c)
{
  if (size != c.size)
  {
    return false;
  }
  for (int i = 0; i < size; i++)
  {
    if (data[i] != c.data[i])
    {
      return false;
    }
  }
  return true;
}

// Overloaded insertion operator
IntCollection &IntCollection::operator<<(int value)
{
  add(value);
  return *this;
}

void IntCollection::addCapacity()
{
  int *newData;
  capacity += CHUNK_SIZE;
  newData = new int[capacity];
  for (int i = 0; i < size; i++)
  {
    newData[i] = data[i];
  }
  delete[] data;
  data = newData;
}

void IntCollection::add(int value)
{
  if (size == capacity)
  {
    addCapacity();
  }
  data[size++] = value;
}

int IntCollection::get(int index)
{
  if (index < 0 || index >= size)
  {
    cout << "ERROR: get() trying to access index out of range.\n";
    exit(1);
  }
  return data[index];
}

int IntCollection::getSize()
{
  return size;
}