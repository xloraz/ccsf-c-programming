// Circle.cpp
#include "Circle.h"
#include <cmath> // For sqrt()

using namespace std;

Circle::Circle(double x, double y, double r) : x(x), y(y), radius(r) {}

void Circle::setRadius(double r)
{
  radius = r;
}

void Circle::setX(double value)
{
  x = value;
}

void Circle::setY(double value)
{
  y = value;
}

double Circle::getRadius() const
{
  return radius;
}

double Circle::getX() const
{
  return x;
}

double Circle::getY() const
{
  return y;
}

double Circle::getArea() const
{
  return radius * radius * 3.14;
}

bool Circle::containsPoint(double xValue, double yValue) const
{
  double distance = sqrt(pow(x - xValue, 2) + pow(y - yValue, 2));
  return distance <= radius;
}