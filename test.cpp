// Discussion 5: Credit Card Fraud
// By San Myo Aung

#include <iostream>
#include <fstream>
using namespace std;

void lucky();

int main()
{
  lucky();
  return 0;
}

void lucky()
{
  fstream inputFile;
  int number;
  bool found = false;

  inputFile.open("cards.txt");

  if (!inputFile)
  {
    cout << "ERROR! Invalid File Name.\n";
    exit(1);
  }

  while (inputFile >> number)
  {
    while (number > 0)
    {
      int digit = number % 10; // Extract the last digit
      if (digit == 7)
      {
        found = true; // If the digit is 7, return true
        break;
      }
      number /= 10; // Remove the last digit from the number
    }
    if (found)
      break;
  }
  if (found)
    cout << "Lucky 7!\n";
  else
    cout << "Better luck next time!\n";

  inputFile.close();
}

/*

*/