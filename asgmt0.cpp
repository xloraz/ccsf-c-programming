// Assignment 0: San Myo Aung
// This program prints out as many asterisks as the user wants.

#include <iostream>
using namespace std;

int main()
{
  int x;
  char tryAgain = 'y';
  while (tryAgain != 'n')
  {
    cout << "How many asterisks?: ";
    cin >> x;
    for (int i = 0; i < x; i++)
    {
      cout << "*";
    }
    cout << endl;
    cout << "Try again? (y/n): ";
    cin >> tryAgain;
  }
  return 0;
}

/*

[saung32@hills ccsf-c-programming]$ ./a.out
How many asterisks?: 3
***
Try again? (y/n): y
How many asterisks?: 2
**
Try again? (y/n): y
How many asterisks?: 1
*
Try again? (y/n): y
How many asterisks?: 5
*****
Try again? (y/n): n
[saung32@hills ccsf-c-programming]$

*/