// Assignment 13: IntCollection
// By San Myo Aung

#include "IntCollection.h"
#include <iostream>
using namespace std;

int main()
{
  // Test basic functionality
  IntCollection c1;
  c1.add(45);
  c1.add(-210);
  c1.add(77);
  c1 << 2 << -21 << 42 << 7;

  cout << "c1: ";
  for (int i = 0; i < c1.getSize(); i++)
  {
    cout << c1.get(i) << " ";
  }
  cout << endl;

  // Test copy constructor
  IntCollection c2(c1);
  cout << "c2 (copy of c1): ";
  for (int i = 0; i < c2.getSize(); i++)
  {
    cout << c2.get(i) << " ";
  }
  cout << endl;

  // Test assignment operator
  IntCollection c3;
  c3 = c1;
  cout << "c3 (assigned from c1): ";
  for (int i = 0; i < c3.getSize(); i++)
  {
    cout << c3.get(i) << " ";
  }
  cout << endl;

  // Test chaining assignment
  IntCollection c4;
  c4 = c3 = c1;
  cout << "c4 (chained assignment): ";
  for (int i = 0; i < c4.getSize(); i++)
  {
    cout << c4.get(i) << " ";
  }
  cout << endl;

  // Test equality operator
  cout << "c1 == c2: " << (c1 == c2) << endl;
  cout << "c1 == c3: " << (c1 == c3) << endl;
  c3.add(100);
  cout << "c1 == c3 (after adding to c3): " << (c1 == c3) << endl;

  // c1.addCapacity(); // This will cause a compiler error because addCapacity() is private

  return 0;
}

/*
[saung32@hills ccsf-c-programming]$ ./a.out
c1: 45 -210 77 2 -21 42 7
c2 (copy of c1): 45 -210 77 2 -21 42 7
c3 (assigned from c1): 45 -210 77 2 -21 42 7
c4 (chained assignment): 45 -210 77 2 -21 42 7
c1 == c2: 1
c1 == c3: 1
c1 == c3 (after adding to c3): 0
[saung32@hills ccsf-c-programming]$

Answer to question 6:
You cannot call addCapacity() from main() because it is a private member function.
Attempting to do so will result in a compiler error.
*/