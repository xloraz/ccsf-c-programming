// Discussion 11: Rectangle improved
// By San Myo Aung

#include <iostream>
#include <cstring>

using namespace std;

class Rectangle
{
private:
  double width;
  double length;
  char *name;
  void initName(char *n);

public:
  // Constructors
  Rectangle();
  Rectangle(double w, double l, char *n);

  // Destructor
  ~Rectangle();

  void setWidth(double w);
  void setLength(double l);
  void setWidth(char *w);
  void setLength(char *l);
  void setName(char *n);
  double getWidth() const;
  double getLength() const;
  void printName() const { cout << name; }
  double getArea() const { return width * length; }
};

// Private member function to initialize name
void Rectangle::initName(char *n)
{
  name = new char[strlen(n) + 1];
  strcpy(name, n);
}

// Constructors
Rectangle::Rectangle() : width(1.0), length(1.0)
{
  initName("Default");
}

Rectangle::Rectangle(double w, double l, char *n) : width(w), length(l)
{
  initName(n);
}

// Destructor
Rectangle::~Rectangle()
{
  delete[] name;
}

// Setters
void Rectangle::setWidth(double w)
{
  width = w;
}

void Rectangle::setLength(double l)
{
  length = l;
}

// These likely should take doubles as arguments, not char*
void Rectangle::setWidth(char *w)
{
  width = atof(w); // Convert char* to double
}

void Rectangle::setLength(char *l)
{
  length = atof(l); // Convert char* to double
}

void Rectangle::setName(char *n)
{
  delete[] name; // Deallocate old memory
  initName(n);   // Allocate new memory and copy name
}

// Getters
double Rectangle::getWidth() const
{
  return width;
}

double Rectangle::getLength() const
{
  return length;
}

int main()
{
  Rectangle house[] = {
      Rectangle(10, 12, (char *)"Kitchen"),
      Rectangle(20, 20, (char *)"Bedroom"),
      Rectangle(8, 12, (char *)"Offce")};

  // Fix the name "Offce"
  house[2].setName((char *)"Office");

  // Find the room with the largest area
  int largestAreaIndex = 0;
  double largestArea = house[0].getArea();
  for (int i = 1; i < 3; i++)
  {
    if (house[i].getArea() > largestArea)
    {
      largestAreaIndex = i;
      largestArea = house[i].getArea();
    }
  }

  // Output room information
  for (int i = 0; i < 3; i++)
  {
    house[i].printName();
    cout << " Area: " << house[i].getArea() << endl;
  }

  cout << "The room with the largest area is the ";
  house[largestAreaIndex].printName();
  cout << endl;

  return 0;
}

/*
[saung32@hills ccsf-c-programming]$ ./a.out
Kitchen Area: 120
Bedroom Area: 400
Office Area: 96
The room with the largest area is the Bedroom
[saung32@hills ccsf-c-programming]$
*/