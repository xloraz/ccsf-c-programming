// Assignment 14 - Random Number Guesser
// By San Myo Aung

#include "RandomNumberGuesser.h"

RandomNumberGuesser::RandomNumberGuesser(int l, int h) : NumberGuesser(l, h), gen(rd()), distrib(l, h)
{
  currentGuess = 0; // Initialize currentGuess in the constructor
}

int RandomNumberGuesser::getCurrentGuess()
{
  if (currentGuess == 0)
  {
    currentGuess = distrib(gen);
  }
  return currentGuess;
}

void RandomNumberGuesser::higher()
{
  low = getCurrentGuess() + 1;
  distrib = std::uniform_int_distribution<>(low, high);
  currentGuess = 0; // Reset for the next guess
}

void RandomNumberGuesser::lower()
{
  high = getCurrentGuess() - 1;
  distrib = std::uniform_int_distribution<>(low, high);
  currentGuess = 0; // Reset for the next guess
}

void RandomNumberGuesser::reset()
{
  NumberGuesser::reset();
  distrib = std::uniform_int_distribution<>(low, high);
  currentGuess = 0; // Reset for the next guess
}