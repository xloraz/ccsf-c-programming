// Assignment 3: San Myo Aung
// Number Guesser

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
using namespace std;

void playOneGame();               // Play Guess Game
bool shouldPlayAgain();           // Ask play again?
char getUserResponseToGuess(int); // Ask higher lower or correct
int getMidpoint(int, int);        // Get the mid point
int getRandomMidpoint(int, int);  // Get the random mid point
void goRandom();                  // Play with random mid point guess

bool doRandom;

int main()
{
  // Get the system time
  unsigned seed = time(0);

  // Seed the number generator
  srand(seed);

  do
  {
    goRandom();
    playOneGame();
  } while (shouldPlayAgain());

  return 0;
}

void playOneGame()
{
  int h = 100, l = 1, m;
  char x;
  cout << "Think of a number between " << l << " and " << h << "." << endl;
  do
  {
    if (doRandom)
      m = getRandomMidpoint(l, h);
    else
      m = getMidpoint(l, h);
    x = getUserResponseToGuess(m);
    if (x == 'h')
      l = m + 1;
    else if (x == 'l')
      h = m - 1;
  } while (x != 'c');
}

bool shouldPlayAgain()
{
  char x;
  cout << "Great! Do you want to play again? (y/n): ";
  cin >> x;
  if (x == 'y')
    return true;
  else
    return false;
}

int getMidpoint(int low, int high)
{
  return ((high - low) / 2) + low;
}

int getRandomMidpoint(int low, int high)
{
  return (rand() % (high - low + 1)) + low;
}

char getUserResponseToGuess(int guess)
{
  char x;
  cout << "Is it " << guess << "?  (h/l/c): ";
  cin >> x;
  return x;
}

void goRandom()
{
  char x;
  cout << "Use Random Guess (y/n): ";
  cin >> x;
  if (x == 'y')
    doRandom = true;
  else
    doRandom = false;
}

/*

[saung32@hills ccsf-c-programming]$ ./a.out
Use Random Guess (y/n): n
Think of a number between 1 and 100.
Is it 50?  (h/l/c): l
Is it 25?  (h/l/c): h
Is it 37?  (h/l/c): l
Is it 31?  (h/l/c): h
Is it 34?  (h/l/c): h
Is it 35?  (h/l/c): c
Great! Do you want to play again? (y/n): y
Use Random Guess (y/n): n
Think of a number between 1 and 100.
Is it 50?  (h/l/c): h
Is it 75?  (h/l/c): h
Is it 88?  (h/l/c): l
Is it 81?  (h/l/c): c
Great! Do you want to play again? (y/n): y
Use Random Guess (y/n): y
Think of a number between 1 and 100.
Is it 29?  (h/l/c): h
Is it 55?  (h/l/c): l
Is it 50?  (h/l/c): l
Is it 36?  (h/l/c): l
Is it 30?  (h/l/c): h
Is it 33?  (h/l/c): h
Is it 35?  (h/l/c): c
Great! Do you want to play again? (y/n): y
Use Random Guess (y/n): y
Think of a number between 1 and 100.
Is it 46?  (h/l/c): h
Is it 77?  (h/l/c): h
Is it 87?  (h/l/c): l
Is it 84?  (h/l/c): l
Is it 79?  (h/l/c): h
Is it 82?  (h/l/c): l
Is it 81?  (h/l/c): c
Great! Do you want to play again? (y/n): y
Use Random Guess (y/n): n
Think of a number between 1 and 100.
Is it 50?  (h/l/c): h
Is it 75?  (h/l/c): h
Is it 88?  (h/l/c): h
Is it 94?  (h/l/c): h
Is it 97?  (h/l/c): h
Is it 99?  (h/l/c): h
Is it 100?  (h/l/c): c
Great! Do you want to play again? (y/n): y
Use Random Guess (y/n): y
Think of a number between 1 and 100.
Is it 31?  (h/l/c): h
Is it 37?  (h/l/c): h
Is it 45?  (h/l/c): h
Is it 84?  (h/l/c): h
Is it 96?  (h/l/c): h
Is it 98?  (h/l/c): h
Is it 99?  (h/l/c): h
Is it 100?  (h/l/c): c
Great! Do you want to play again? (y/n): n
[saung32@hills ccsf-c-programming]$

*/