// Circle.h
#ifndef CIRCLE_H
#define CIRCLE_H

class Circle
{
private:
  double x;
  double y;
  double radius;

public:
  Circle(double x = 0.0, double y = 0.0, double r = 1.0); // Constructor
  void setRadius(double r);
  void setX(double value);
  void setY(double value);
  double getRadius() const;
  double getX() const;
  double getY() const;
  double getArea() const;
  bool containsPoint(double xValue, double yValue) const;
};

#endif