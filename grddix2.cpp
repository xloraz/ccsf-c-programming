// Discussion 2: Looping
// By San Myo Aung

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
  const int SCORE_A = 90,
            SCORE_B = 80,
            SCORE_C = 70,
            SCORE_D = 60,
            MAX_SCORE = 100,
            MIN_SCORE = 0;

  int xAsgmt, xScore, totalScore = 0;
  float avgScore;
  cout << "How many assignment are there?: ";
  cin >> xAsgmt;
  cout << "----------------------------------\n\n";
  for (int i = 1; i <= xAsgmt; i++)
  {
    cout << "What is your score for Assignment " << i << "?: ";
    cin >> xScore;
    if (xScore >= MIN_SCORE && xScore <= MAX_SCORE)
    {
      totalScore += xScore;
      cout << endl;
    }
    else
    {
      cout << "-------\n"
           << "That is an invalid score.\n"
           << "enter a value in the range of\n"
           << MIN_SCORE << " through " << MAX_SCORE << ".\n"
           << "Try again!\n"
           << "-------\n\n";
      i--;
    }
  }
  cout << "----------------------------------\n\n";

  avgScore = (float)totalScore / xAsgmt;
  cout << "Total Score: " << totalScore << endl;
  cout << "Average Score: " << setprecision(4) << avgScore << endl;
  if (avgScore >= SCORE_A)
    cout << "Your Grade is: A. \n";
  else if (avgScore >= SCORE_B)
    cout << "Your Grade is: B. \n";
  else if (avgScore >= SCORE_C)
    cout << "Your Grade is: C. \n";
  else if (avgScore >= SCORE_D)
    cout << "Your Grade is: D. \n";
  else
    cout << "Your Grade is: F. \n";
  return 0;
}

/*

[saung32@hills ccsf-c-programming]$ ./a.out
How many assignment are there?: 3
----------------------------------

What is your score for Assignment 1?: 95

What is your score for Assignment 2?: 90

What is your score for Assignment 3?: -1
-------
That is an invalid score.
enter a value in the range of
0 through 100.
Try again!
-------

What is your score for Assignment 3?: 120
-------
That is an invalid score.
enter a value in the range of
0 through 100.
Try again!
-------

What is your score for Assignment 3?: 93

----------------------------------

Total Score: 278
Average Score: 92.67
Your Grade is: A.
[saung32@hills ccsf-c-programming]$

*/