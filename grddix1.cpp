// Discussion 1: San Myo Aung

#include <iostream>
using namespace std;

int main()
{
  int numInnings, numRuns;
  float ERA;
  cout << "How many innings did the pitcher pitch today?: ";
  cin >> numInnings;
  cout << "How many runs did the pitcher give up today?: ";
  cin >> numRuns;
  ERA = (float)numRuns * 9 / numInnings;
  cout << "Pitcher's ERA: " << ERA;
  cout << endl;
  return 0;
}

/*

[saung32@hills ccsf-c-programming]$ ./a.out
How many innings did the pitcher pitch today?: 4
How many runs did the pitcher give up today?: 2
Pitcher's ERA: 4.5
[saung32@hills ccsf-c-programming]$ ./a.out
How many innings did the pitcher pitch today?: 3
How many runs did the pitcher give up today?: 5
Pitcher's ERA: 15
[saung32@hills ccsf-c-programming]$ ./a.out
How many innings did the pitcher pitch today?: 4
How many runs did the pitcher give up today?: 1
Pitcher's ERA: 2.25
[saung32@hills ccsf-c-programming]$

*/