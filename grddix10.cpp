// Discussion 10: Rectangular Class
// By San Myo Aung

#include <iostream>
#include <string>

using namespace std;

class Rectangle
{
private:
  double width;
  double length;

public:
  Rectangle(double w = 1.0, double l = 1.0); // Constructor
  void setWidth(double w);
  void setLength(double l);
  double getWidth() const;
  double getLength() const;
  double getArea() const;
  double getPerimeter() const; // New function
  bool isSquare() const;       // New function
};

Rectangle::Rectangle(double w, double l) : width(w), length(l) {}

void Rectangle::setWidth(double w)
{
  width = w;
}

void Rectangle::setLength(double l)
{
  length = l;
}

double Rectangle::getWidth() const
{
  return width;
}

double Rectangle::getLength() const
{
  return length;
}

double Rectangle::getArea() const
{
  return width * length;
}

double Rectangle::getPerimeter() const
{
  return 2 * width + 2 * length;
}

bool Rectangle::isSquare() const
{
  return width == length;
}

int main()
{
  double width = 0, length = 0;
  cout << "Enter the width : ";
  cin >> width;
  cout << "Enter the length : ";
  cin >> length;
  Rectangle rect(width, length);

  cout << "Width: " << rect.getWidth() << endl;
  cout << "Length: " << rect.getLength() << endl;
  cout << "Area: " << rect.getArea() << endl;
  cout << "Perimeter: " << rect.getPerimeter() << endl;
  cout << "Is Square? " << (rect.isSquare() ? "Yes" : "No") << endl;

  return 0;
}

/*
[saung32@hills ccsf-c-programming]$ ./a.out
Enter the width : 5
Enter the length : 5
Width: 5
Length: 5
Area: 25
Perimeter: 20
Is Square? Yes
[saung32@hills ccsf-c-programming]$ ./a.out
Enter the width : 5
Enter the length : 4
Width: 5
Length: 4
Area: 20
Perimeter: 18
Is Square? No
[saung32@hills ccsf-c-programming]$
*/