// Assignment 5: San Myo Aung
// The League

#include <iostream>
#include <iomanip>
using namespace std;

void initializeArrays(string *, int *, int);
void sortData(string *, int *, int);
void displayData(string *, int *, int);
void swap(int *, int *);
void swap(string *, string *);

int main()
{
  int NUM;
  cout << "How many team will you enter?" << endl;
  cin >> NUM;
  string *names = new string[NUM];
  int *wins = new int[NUM];
  initializeArrays(names, wins, NUM);
  sortData(names, wins, NUM);
  displayData(names, wins, NUM);
  delete[] names;
  delete[] wins;
  return 0;
}

void initializeArrays(string *names, int *wins, int size)
{
  for (int i = 0; i < size; i++)
  {
    cout << "Enter team #" << i + 1 << ": ";
    cin >> names[i];
    cout << "Enter the wins for team #" << i + 1 << ": ";
    cin >> wins[i];
  }
}

void sortData(string *names, int *wins, int size)
{
  int s, maxIndex, maxValue;

  for (s = 0; s < (size - 1); s++)
  {
    maxIndex = s;
    maxValue = wins[s];
    for (int i = s + 1; i < size; i++)
    {
      if (wins[i] > maxValue)
      {
        maxValue = wins[i];
        maxIndex = i;
      }
    }
    swap(&wins[maxIndex], &wins[s]);
    swap(&names[maxIndex], &names[s]);
  }
}

void displayData(string *names, int *wins, int size)
{
  cout << "\nLeague Standings:\n";
  for (int i = 0; i < size; i++)
  {
    cout << names[i] << ": " << wins[i] << endl;
  }
}

void swap(int *a, int *b)
{
  int temp = *a;
  *a = *b;
  *b = temp;
}

void swap(string *a, string *b)
{
  string temp = *a;
  *a = *b;
  *b = temp;
}
/*
[saung32@hills ccsf-c-programming]$ g++ leagueDMA.cpp
[saung32@hills ccsf-c-programming]$ ./a.out
How many team will you enter?
5
Enter team #1: One
Enter the wins for team #1: 75
Enter team #2: Two
Enter the wins for team #2: 91
Enter team #3: Three
Enter the wins for team #3: 65
Enter team #4: Four
Enter the wins for team #4: 92
Enter team #5: Five
Enter the wins for team #5: 85

League Standings:
Four: 92
Two: 91
Five: 85
One: 75
Three: 65
[saung32@hills ccsf-c-programming]$
*/