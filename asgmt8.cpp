// Assignment 8: C-Strings
// By San Myo Aung

#include <iostream>
#include <cstring>

using namespace std;

// Find the last index of a character in a string.
int lastIndexOf(char *s, char target)
{
  int lastIndex = -1;
  int i = 0;

  while (s[i] != '\0')
  {
    if (s[i] == target)
    {
      lastIndex = i;
    }
    i++;
  }

  return lastIndex;
}

// Reverse a string in place.
void reverse(char *s)
{
  int start = 0;
  int end = strlen(s) - 1;

  while (start < end)
  {
    char temp = s[start];
    s[start] = s[end];
    s[end] = temp;
    start++;
    end--;
  }
}

// Replace all instances of a character with another character.
int replace(char *s, char target, char replacementChar)
{
  int count = 0;
  int i = 0;

  while (s[i] != '\0')
  {
    if (s[i] == target)
    {
      s[i] = replacementChar;
      count++;
    }
    i++;
  }

  return count;
}

// Find the index of the first occurrence of a substring.
int findSubstring(char *s, char substring[])
{
  int sLen = strlen(s);
  int subLen = strlen(substring);

  for (int i = 0; i <= sLen - subLen; i++)
  {
    bool found = true;
    for (int j = 0; j < subLen; j++)
    {
      if (s[i + j] != substring[j])
      {
        found = false;
        break;
      }
    }
    if (found)
    {
      return i;
    }
  }

  return -1;
}

// Check if a string is a palindrome.
bool isPalindrome(char *s)
{
  int start = 0;
  int end = strlen(s) - 1;

  while (start < end)
  {
    if (s[start] != s[end])
    {
      return false;
    }
    start++;
    end--;
  }

  return true;
}

// Reverse the words in a string (Extra Credit).
void reverseWords(char *s)
{
  // Reverse the entire string
  reverse(s);

  // Reverse each individual word
  int start = 0;
  int end = 0;
  int len = strlen(s);

  while (start < len)
  {
    while (end < len && s[end] != ' ')
    {
      end++;
    }
    // Reverse the word from start to end-1
    for (int i = start, j = end - 1; i < j; i++, j--)
    {
      char temp = s[i];
      s[i] = s[j];
      s[j] = temp;
    }
    start = end + 1;
    end = start;
  }
}

int main()
{
  char s1[] = "Giants";
  char s2[] = "flower";
  char s3[] = "go giants";
  char s4[] = "Skyscraper";
  char s5[] = "abba";
  char s6[] = "The Giants won the Pennant!";
  char s7[] = "ysc";

  cout << "lastIndexOf(s1, 'a'): " << lastIndexOf(s1, 'a') << endl;

  reverse(s2);
  cout << "reverse(s2): " << s2 << endl;

  cout << "replace(s3, 'g', 'G'): " << replace(s3, 'g', 'G') << endl;
  cout << "s3 after replace: " << s3 << endl;

  cout << "findSubstring(s4, \"ysc\"): " << findSubstring(s4, s7) << endl;

  cout << "isPalindrome(s5): " << isPalindrome(s5) << endl;

  reverseWords(s6);
  cout << "reverseWords(s6): " << s6 << endl;

  return 0;
}

/*
[saung32@hills ccsf-c-programming]$ ./a.out
lastIndexOf(s1, 'a'): 2
reverse(s2): rewolf
replace(s3, 'g', 'G'): 2
s3 after replace: Go Giants
findSubstring(s4, "ysc"): 2
isPalindrome(s5): 1
reverseWords(s6): Pennant! the won Giants The
[saung32@hills ccsf-c-programming]$

*/