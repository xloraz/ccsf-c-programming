// Discussion 5: Credit Card Fraud
// By San Myo Aung

#include <iostream>
#include <fstream>
using namespace std;

bool linearSearch(const int[], int, int);

int main()
{
  fstream inputFile;
  const int NUM = 20;
  int creditCards[NUM];
  int number;
  int i = 0;
  bool fraud = false;

  inputFile.open("cards.txt");

  if (!inputFile)
  {
    cout << "ERROR! Invalid File Name.\n";
    exit(1);
  }

  while (inputFile >> creditCards[i])
  {
    i++;
  }

  do
  {
    cout << "Enter your credit card number: ";
    cin >> number;
    if (linearSearch(creditCards, NUM, number))
    {
      cout << "FRAUD!!!" << endl;
      fraud = true;
    }
    else
    {
      cout << "Thank you!" << endl;
    }
  } while (!fraud);

  return 0;
}

bool linearSearch(const int arr[], int size, int value)
{
  int index = 0;
  int position = -1;
  bool found = false;

  while (index < size && !found)
  {
    if (arr[index] == value)
    {
      found = true;
      position = index;
    }
    index++;
  }
  return found;
}

/*

*/