// Assignment 4: San Myo Aung
// Poker Hands

#include <iostream>
#include <vector>
using namespace std;

const int NUM_CARD = 5;
int low = 0;
bool containsPair(int hand[]);
bool containsTwoPair(int hand[]);
bool containsThreeOfaKind(int hand[]);
bool containsStraight(int hand[]);
bool containsFullHouse(int hand[]);
bool containsFourOfaKind(int hand[]);
bool cardCheck(int);          // to check 2 - 9 only
int checkLowCard(int hand[]); // to check the lowest card number

int main()
{
  int n = 1;
  int hand[NUM_CARD];
  // bool flag = true;
  cout << "Enter five numeric cards, no face cards. Use 2 - 9." << endl;
  for (int x = 0; x < NUM_CARD; x++)
  {
    do
    {
      cout << "Card " << n << ": ";
      cin >> hand[x];
    } while (!cardCheck(hand[x]));
    n++;
  }

  // Sorting the array
  int shand[NUM_CARD]; // To store sorted array
  int s = 0;
  low = checkLowCard(hand);
  int slow = low;
  do
  {
    for (int x = 0; x < NUM_CARD; x++)
    {
      if (slow == hand[x])
      {
        shand[s] = slow;
        s++;
      }
    }
    slow++;
  } while (s < 5);
  // Sorting Ends

  if (containsFourOfaKind(shand))
    cout << "Four of a kind!" << endl;
  else if (containsFullHouse(shand))
    cout << "Full House!" << endl;
  else if (containsStraight(shand))
    cout << "Straight!" << endl;
  else if (containsThreeOfaKind(shand))
    cout << "Three of a kind!" << endl;
  else if (containsTwoPair(shand))
    cout << "Two Pair!" << endl;
  else if (containsPair(shand))
    cout << "Pair!" << endl;
  else
    cout << "High Card!" << endl;
  return 0;
}

bool containsPair(int hand[])
{
  bool z = false;
  for (int x = 0; x < NUM_CARD; x++)
  {

    for (int y = x + 1; y < NUM_CARD; y++)
    {
      if (hand[x] == hand[y])
        return true;
    }
  }
  return z;
}

bool containsTwoPair(int hand[])
{
  int z = 0;
  int firstPair = 0;
  for (int x = 0; x < NUM_CARD; x++)
  {

    for (int y = x + 1; y < NUM_CARD; y++)
    {
      if (hand[x] == hand[y] && hand[x] != firstPair)
      {
        firstPair = hand[x];
        z++;
        break;
      }
    }
  }
  if (z > 1)
    return true;
  else
    return false;
}

bool containsThreeOfaKind(int hand[])
{
  int z = 0;
  for (int x = 0; x < NUM_CARD - 2; x++)
  {
    for (int y = x + 1; y < NUM_CARD; y++)
    {
      if (hand[x] == hand[y])
      {
        z++;
      }
    }
    if (z > 1)
      break;
    else
      z = 0;
  }
  if (z > 1)
    return true;
  else
    return false;
}

bool containsStraight(int hand[])
{
  int s = 1;
  do
  {
    if ((low + s) == hand[s])
      s++;
    else
      break;
  } while (s < 5);
  if (s == 5)
    return true;
  else
    return false;
}

bool containsFullHouse(int hand[])
{
  int card1 = hand[0], card2 = 0, s = 0;

  for (int x = 0; x < NUM_CARD; x++)
  {
    if (card1 == hand[x] || card2 == hand[x])
    {
      s++;
    }
    else if (s < 4 && card2 == 0)
    {
      card2 = hand[x];
      s++;
    }
    else
    {
      break;
    }
  }
  if (s == 5)
    return true;
  else
    return false;
}

bool containsFourOfaKind(int hand[])
{
  int s;
  if (hand[0] == hand[1])
  {
    s = 1;
    for (int x = 2; x < NUM_CARD; x++)
    {
      if (hand[0] == hand[x])
        s++;
      else
        break;
    }
  }
  else
  {
    s = 0;
    for (int x = 2; x < NUM_CARD; x++)
    {
      if (hand[1] == hand[x])
        s++;
      else
        break;
    }
  }
  if (s == 3)
    return true;
  else
    return false;
}

bool cardCheck(int x)
{
  if (x >= 2 && x <= 9)
    return true;
  else
  {
    cout << "Use 2 - 9 only!" << endl;
    return false;
  }
}

int checkLowCard(int hand[])
{
  int card = 0, lowCard = 2;
  do
  {
    for (int x = 0; x < NUM_CARD; x++)
    {
      if (lowCard == hand[x])
      {
        card = lowCard;
        break;
      }
    }
    lowCard++;
  } while (card == 0);
  return card;
}

/*

[saung32@hills ccsf-c-programming]$ ./a.out
Enter five numeric cards, no face cards. Use 2 - 9.
Card 1: 2
Card 2: 5
Card 3: 7
Card 4: 8
Card 5: 2
Pair!
[saung32@hills ccsf-c-programming]$ ./a.out
Enter five numeric cards, no face cards. Use 2 - 9.
Card 1: 2
Card 2: 2
Card 3: 7
Card 4: 8
Card 5: 2
Three of a kind!
[saung32@hills ccsf-c-programming]$ ./a.out
Enter five numeric cards, no face cards. Use 2 - 9.
Card 1: 2
Card 2: 7
Card 3: 7
Card 4: 2
Card 5: 2
Full House!
[saung32@hills ccsf-c-programming]$ ./a.out
Enter five numeric cards, no face cards. Use 2 - 9.
Card 1: 5
Card 2: 2
Card 3: 5
Card 4: 2
Card 5: 5
Full House!
[saung32@hills ccsf-c-programming]$ ./a.out
Enter five numeric cards, no face cards. Use 2 - 9.
Card 1: 2
Card 2: 6
Card 3: 2
Card 4: 2
Card 5: 2
Four of a kind!
[saung32@hills ccsf-c-programming]$ ./a.out
Enter five numeric cards, no face cards. Use 2 - 9.
Card 1: 7
Card 2: 4
Card 3: 6
Card 4: 5
Card 5: 8
Straight!
[saung32@hills ccsf-c-programming]$ ./a.out
Enter five numeric cards, no face cards. Use 2 - 9.
Card 1: 4
Card 2: 6
Card 3: 3
Card 4: 6
Card 5: 4
Two Pair!
[saung32@hills ccsf-c-programming]$ ./a.out
Enter five numeric cards, no face cards. Use 2 - 9.
Card 1: 4
Card 2: 9
Card 3: 6
Card 4: 2
Card 5: 4
Pair!
[saung32@hills ccsf-c-programming]$ ./a.out
Enter five numeric cards, no face cards. Use 2 - 9.
Card 1: 7
Card 2: 9
Card 3: 5
Card 4: 6
Card 5: 3
High Card!
[saung32@hills ccsf-c-programming]$

*/